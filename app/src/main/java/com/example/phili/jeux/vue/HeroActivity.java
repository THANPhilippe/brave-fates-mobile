package com.example.phili.jeux.vue;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.example.phili.jeux.controleur.Controle;
import com.example.phili.jeux.modele.Monster;
import com.example.phili.jeux.modele.Profil;
import com.example.phili.jeux.R;

import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import pl.droidsonroids.gif.GifImageView;

public class HeroActivity extends AppCompatActivity {

    private Controle controle;
    private TextView vie;
    private TextView viem;
    private TextView force;
    private TextView infodebut;
    private TextView defense;
    private TextView experience;
    private ScrollView scroll;
    private TextView level;
    private TextView levelm;
    private ImageView monster;
    private TextView floor;
    private GifImageView sprite;

    private int viebase;
    private int idPerso;

    public Monster spider = new Monster ("spider",50,1,10,12,8, "spider", new Date());
    public Monster slime = new Monster ("slime",25,1,12,12,10, "slime", new Date());
    public Monster tree = new Monster ("tree",100,5,20,15,15, "tree", new Date());

    public Profil hero;
    Monster listeMonstre[] =  {spider, slime};
    private ArrayList<Monster> lesMonstres = new ArrayList<Monster>();
    public Monster leMonstre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hero);
        init();
    }

    public void init(){
        SharedPreferences editor = getSharedPreferences("MES_PREFS",MODE_PRIVATE);
        idPerso = editor.getInt("Personnage",0);

        if(hero == null){
            hero = new Profil (0,1,20,15,10,1, new Date());
        }
        this.vie = (TextView) findViewById(R.id.vie) ;
        this.force = (TextView) findViewById(R.id.force) ;
        this.defense = (TextView) findViewById(R.id.defense) ;
        this.experience = (TextView) findViewById(R.id.experience) ;
        this.viem = (TextView) findViewById(R.id.viem) ;
        this.floor = (TextView) findViewById(R.id.floor) ;
        this.level = (TextView) findViewById(R.id.level) ;
        this.levelm = (TextView) findViewById(R.id.levelm) ;
        this.monster = (ImageView) findViewById(R.id.monster);
        this.sprite = (GifImageView) findViewById(R.id.sprite);
        this.infodebut = (TextView) findViewById(R.id.info) ;
        vie.setText(""+ hero.getVie() );
        force.setText(""+ hero.getForce() );
        defense.setText(""+ hero.getDefense() );
        experience.setText(""+ 0 );
        level.setText(""+1);
        floor.setText(""+1);
        this.controle = Controle.getInstance(this, idPerso);
        if(leMonstre == null) {
            ajoutMonstre();
        }
        infodebut.setText("Début de la partie");
        infodebut.setMovementMethod(new ScrollingMovementMethod());
        attaque();
        soin();
    }

    /**
     * Ajout d'un nouveau monstre
     */
    private void ajoutMonstre(){
        lesMonstres.clear();
        int x;
        x = (int) (Math.random() * 2);
        lesMonstres.add(listeMonstre[x]);
        leMonstre = lesMonstres.get(0);

        viebase = leMonstre.getVie();
        controle.creerMonstre(leMonstre.getNom(), leMonstre.getExp(), leMonstre.getLevel(), leMonstre.getVie(), leMonstre.getForce(), leMonstre.getDefense(), leMonstre.getImage(), this);
        this.controle.evenementModele("ajout monstre", leMonstre);
    }

    /**
     * Ajout d'un boss
     */
    private void ajoutBoss(){
        lesMonstres.clear();
        if(hero.getFloor() == 10){
            leMonstre = tree;
        }
        viebase = leMonstre.getVie();
        controle.creerMonstre(leMonstre.getNom(), leMonstre.getExp(), leMonstre.getLevel(), leMonstre.getVie(), leMonstre.getForce(), leMonstre.getDefense(), leMonstre.getImage(), this);
        this.controle.evenementModele("ajout monstre", leMonstre);
        infodebut.append("\n Un boss apparaît!");
    }

    /**
     * Affiche les informations d'un nouveau monstre
     * @param unMonstre
     */
    public void infoMonstre(Monster unMonstre){
        int id = getResources().getIdentifier(unMonstre.getImage(), "drawable", getPackageName());
        viem.setText(""+ unMonstre.getVie());
        levelm.setText(""+ unMonstre.getLevel());
        monster.setImageResource(id);
    }

    /**
     * Affiche les informations d'un nouveau profil
     * @param profil
     */
    public void newProfil(Profil profil){
        this.hero = profil;
        vie.setText(""+ hero.getVie() );
        force.setText(""+ hero.getForce() );
        defense.setText(""+ hero.getDefense() );
        experience.setText(""+ 0 );
        level.setText(""+1);
        floor.setText(""+1);
    }

    /**
     * Affiche les informations du dernier profil sauvegardé
     * @param profil
     */
    public void oldProfil(Profil profil){
        this.hero = profil;
        if(hero.getExperience() == 100 || hero.getExperience() > 100){
            hero.setLevel(hero.getLevel()+1);
            hero.setExperience(0);
        }
        vie.setText(""+ hero.getVie() );
        force.setText(""+ hero.getForce() );
        defense.setText(""+ hero.getDefense() );
        experience.setText(""+ hero.getExperience() );
        level.setText(""+hero.getLevel());
        floor.setText(""+hero.getFloor());
    }

    /**
     * Affiche les informations du dernier monstre sauvegardé
     * @param monstre
     */
    public void oldMonster(Monster monstre){
        this.leMonstre = monstre;
        int id = getResources().getIdentifier(leMonstre.getImage(), "drawable", getPackageName());
        viem.setText(""+ leMonstre.getVie());
        levelm.setText(""+ leMonstre.getLevel());
        monster.setImageResource(id);
    }


    private void attaque() {
        ((Button) findViewById(R.id.attaquer)).setOnClickListener(new Button.OnClickListener(){
            public void onClick(View v) {
                sprite.setBackgroundResource(R.drawable.michele);
                calculDegats(leMonstre,0);
            }
        });
    }

    private void soin(){
        ((Button) findViewById(R.id.soin)).setOnClickListener(new Button.OnClickListener(){
            public void onClick(View v) {
                hero.setVie(hero.getVie()+10);
                vie.setText(""+hero.getVie());
                infodebut.append("\n Vous récupérez de 10PV!");
                calculDegats(leMonstre,1);
            }
        });
    }

    private void calculDegats(Monster unMonstre, int soin){
        int degats = hero.getForce() - unMonstre.getDefense(); //dégâts infligé par le joueur
        int subis = unMonstre.getForce() - hero.getDefense(); //dégats subis par le joueur

        Random randomGenerator = new Random();

        if(soin == 0) {
            if(randomGenerator.nextInt(11) == 2){
                infodebut.append("\n Coup critique! Vous infligez plus de dégâts!");
                degats += 5;
            }

            if(randomGenerator.nextInt(11) == 1){
                infodebut.append("\n Esquive! Vous subissez moins de dégâts!");
                subis += -5;
            }

            if (degats > unMonstre.getVie()) {
                unMonstre.setVie(0);
            } else {
                infodebut.append("\n Le monstre a subi " + degats + " dégâts!");
                unMonstre.setVie(unMonstre.getVie() - degats);
                viem.setText("" + (unMonstre.getVie()));
            }
        }
        if(subis <0 || unMonstre.getVie() == 0){
        }

        else{
            infodebut.append("\n Vous avez subi "+subis+ " dégâts!");
            hero.setVie(hero.getVie() - subis);
            vie.setText(""+hero.getVie());
        }

        if(hero.getVie() ==0 || hero.getVie() < 0){ //Si le joueur est mort
            infodebut.append("\n Vous êtes mort...");
            unMonstre.setVie(viebase);
            this.controle.evenementModele("nouveau profil", new Profil (0,1,20,15,10,1, new Date())); //Création d'un nouveau profil
            ajoutMonstre();
        }

        if(unMonstre.getVie() == 0 || unMonstre.getVie() < 0){ //Si le monstre est mort
            infodebut.append("\n Monstre vaincu!");
            infodebut.append("\n Vous gagnez "+unMonstre.getExp()+" EXP");
            unMonstre.setVie(viebase);
            hero.setExperience(hero.getExperience() + unMonstre.getExp());
            experience.setText(""+ (hero.getExperience()));
            hero.setFloor(hero.getFloor()+1);
            floor.setText(""+hero.getFloor());
            if(hero.getFloor() == 10){
                ajoutBoss();
            }else{
                ajoutMonstre();
            }
        }

        if(hero.getExperience() == 100 || hero.getExperience() > 100){
            infodebut.append("\n Level up!");
            hero.setLevel(hero.getLevel()+1);
            level.setText(""+hero.getLevel());
            hero.setExperience(0);
            experience.setText(""+0);
            int x = (int) (Math.random() * 2);
            int y = (int) (Math.random() * 2);
            if(x == 1){
                hero.setForce(hero.getForce()+1);
                force.setText(""+hero.getForce());
            }
            if(y == 1){
                hero.setDefense(hero.getDefense()+1);
                defense.setText(""+hero.getDefense());
            }

        }
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Quitter et sauvegarder la partie?")
                .setCancelable(false)
                .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        controle.sauvegarderProfil(hero.getExperience(),hero.getLevel(),hero.getVie(),hero.getForce(),hero.getDefense(),hero.getFloor(),idPerso, HeroActivity.this);
                        controle.sauvegarderMonstre(leMonstre.getNom(), leMonstre.getExp(), leMonstre.getLevel(), leMonstre.getVie(), leMonstre.getForce(), leMonstre.getDefense(), leMonstre.getImage(),idPerso, HeroActivity.this);
                        Intent intent = new Intent(HeroActivity.this, MainActivity.class);
                        startActivity(intent);
                        HeroActivity.this.finish();
                    }
                })
                .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

}
