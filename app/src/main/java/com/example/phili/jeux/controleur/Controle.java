package com.example.phili.jeux.controleur;

import android.content.Context;
import android.provider.Settings;
import com.example.phili.jeux.modele.AccesMongo;
import com.example.phili.jeux.modele.Monster;
import com.example.phili.jeux.modele.Profil;
import com.example.phili.jeux.vue.HeroActivity;

import java.util.Date;

/**
 * Created by phili on 24/12/2016.
 */

public final class Controle {

    private static Controle instance = null;
    private static Monster monstre;
    private static Profil profil;
    private static Context contexte;
    private static AccesMongo accesMongo;


    private Controle(){
        super();
    }

    public static final Controle getInstance(Context context, int idPerso){

        Controle.instance = new Controle();
        accesMongo = new AccesMongo(context);
        profil = accesMongo.getLastProfil(Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID), idPerso);
        monstre = accesMongo.getLastMonstre(Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID), idPerso);
        contexte = context;
        if(profil != null) {
            setOldProfil(profil);
        }
        if(monstre != null) {
            setOldMonster(monstre);
        }
        return Controle.instance;
    }

    /**
     * Affiche les informations d'un nouveau profil
     * @param profil
     */
    public void setNewProfil (Profil profil){
        this.profil = profil;
        ((HeroActivity)contexte).newProfil(profil);
    }

    /**
     * Affiche les informations d'un profil sauvegardé
     * @param profil
     */
    public static void setOldProfil (Profil profil){
        ((HeroActivity)contexte).oldProfil(profil);
    }

    /**
     * Affiche les informations d'un monstre
     * @param monstre
     */
    public void setInfo(Monster monstre){
        this.monstre = monstre;
        ((HeroActivity)contexte).infoMonstre(monstre);
    }

    /**
     * Affiche les informations d'un monstre sauvegardé
     * @param monstre
     */
    public static void setOldMonster(Monster monstre){
        ((HeroActivity)contexte).oldMonster(monstre);
    }

    public void evenementModele(String ordre, Object info) {
        if(ordre == "ajout monstre"){
            setInfo((Monster)info);
        }
        if(ordre == "nouveau profil"){
            setNewProfil((Profil)info);
        }
    }

    /**
     * Création puis ajout dans la BDD d'un profil
     * @param experience
     * @param level
     * @param vie
     * @param force
     * @param defense
     * @param floor
     * @param context
     */
    public void sauvegarderProfil(int experience, int level, int vie, int force, int defense, int floor, int idperso, Context context){
        profil = new Profil(experience, level, vie, force, defense, floor, new Date());
        accesMongo.ajout(profil, Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID), idperso);
    }

    /**
     * Création puis ajout dans la BDD d'un monstre
     * @param nom
     * @param exp
     * @param level
     * @param vie
     * @param force
     * @param defense
     * @param image
     * @param context
     */
    public void creerMonstre(String nom, int exp, int level, int vie, int force, int defense, String image, Context context){
        monstre = new Monster(nom, exp, level, vie, force, defense, image, new Date());
    }

    public void sauvegarderMonstre(String nom, int exp, int level, int vie, int force, int defense, String image, int idperso, Context context){
        monstre = new Monster(nom, exp, level, vie, force, defense, image, new Date());
        accesMongo.ajoutMonstre(monstre,Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID), idperso);
    }

}
