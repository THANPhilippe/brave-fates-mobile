package com.example.phili.jeux.modele;

import android.content.Context;
import android.os.AsyncTask;

import com.example.phili.jeux.outils.Utils;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;


import org.bson.Document;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.ExecutionException;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.combine;
import static com.mongodb.client.model.Updates.set;

public class AccesMongo {

    private MongoDatabase database;
    private MongoCollection<Document> monstres;
    private MongoCollection<Document> profils;
    private Document docP;
    private Document docM;
    private String id;
    private int idPerso;
    private Profil profil;
    private String urlDB;
    private Monster monstre;

    public AccesMongo(Context context){

        try {
            urlDB = Utils.getProperty("dbmongo",context);
        } catch (IOException e) {
            e.printStackTrace();
        }

        MongoClientURI uri = new MongoClientURI(urlDB);
        MongoClient mongoClient = new MongoClient(uri);
        MongoDatabase database = mongoClient.getDatabase("test");
        database = mongoClient.getDatabase("jeux");
        monstres  = database.getCollection("monstres");
        profils  = database.getCollection("profil");

    }

    /**
     * Récupérer le dernier profil du joueur
     * @param id
     * @return
     */
    public Profil getLastProfil(String id, int idPerso){
        this.id = id;
        this.idPerso = idPerso;
        TransactionMongo transaction = new TransactionMongo();
        try {
            String result = transaction.execute("getProfil").get(); //Attendre jusqu'à ce que la tâche soit terminé
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return profil;
    }

    /**
     * Récupérer le dernier monstre que le joueur a affronté
     * @param id
     * @return
     */
    public Monster getLastMonstre(String id, int idPerso){
        this.id = id;
        this.idPerso = idPerso;
        TransactionMongo transaction = new TransactionMongo();
        try {
            String result = transaction.execute("getMonstre").get(); //Attendre jusqu'à ce que la tâche soit terminé
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return monstre;
    }

    /**
     * Ajouter/Maj du dernier monstre que le joueur a affronté
     * @param monstre
     * @param id
     */
    public void ajoutMonstre(Monster monstre, String id, int idPerso){
        this.id = id;
        this.idPerso = idPerso;
        this.monstre = monstre;

        docM = new Document();

        docM.append("idPhone", id)
                .append("idPerso", idPerso)
                .append("nom", monstre.getNom())
                .append("experience", monstre.getExp())
                .append("niveau", monstre.getLevel())
                .append("vie", monstre.getVie())
                .append("force", monstre.getForce())
                .append("defense", monstre.getDefense())
                .append("image", monstre.getImage())
                .append("date", monstre.getDateCreation());

        TransactionMongo transaction = new TransactionMongo();
        transaction.execute("ajoutM");
    }

    /**
     * Ajout/Maj du profil du joueur
     * @param profil
     * @param id
     */
    public void ajout(Profil profil, String id, int idPerso){
        this.id = id;
        this.idPerso = idPerso;
        this.profil = profil;
        docP = new Document();

        docP.append("idPhone", id)
                .append("idPerso", idPerso)
                .append("date", profil.getDateCreation())
                .append("experience", profil.getExperience())
                .append("niveau", profil.getLevel())
                .append("vie", profil.getVie())
                .append("force", profil.getForce())
                .append("defense", profil.getDefense())
                .append("etage", profil.getFloor());

        TransactionMongo transaction = new TransactionMongo();
        transaction.execute("ajout");
    }

    private class TransactionMongo extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String[] params) {

            if(params[0] == "ajout"){
                if(profils.count(and(eq("idPhone", id),eq("idPerso", idPerso))) == 0){
                    profils.insertOne(docP);
                }else{
                    profils.updateOne(
                            and(
                            eq("idPhone", id),eq("idPerso", idPerso)),
                            combine(
                                    set("idPhone", id)
                                    ,set("date", profil.getDateCreation())
                                    ,set("experience", profil.getExperience())
                                    ,set("niveau", profil.getLevel())
                                    ,set("vie", profil.getVie())
                                    ,set("force", profil.getForce())
                                    ,set("defense", profil.getDefense())
                                    ,set("etage", profil.getFloor())
                            ));
                }
            }

            if(params[0] == "ajoutM"){
                if(monstres.count(and(eq("idPhone", id),eq("idPerso", idPerso))) == 0){
                    monstres.insertOne(docM);
                }else{
                    monstres.updateOne(
                            and(
                                    eq("idPhone", id),eq("idPerso", idPerso)),
                            combine(
                                    set("idPhone", id)
                                    ,set("nom", monstre.getNom())
                                    ,set("experience", monstre.getExp())
                                    ,set("niveau", monstre.getLevel())
                                    ,set("vie", monstre.getVie())
                                    ,set("force", monstre.getForce())
                                    ,set("defense", monstre.getDefense())
                                    ,set("image", monstre.getImage())
                                    ,set("date", monstre.getDateCreation())
                            ));
                }
            }

            if(params[0] == "getProfil"){

                Document document = profils.find(and(eq("idPhone", id),eq("idPerso", idPerso))).first();

                if(document != null) {
                    Profil recup = new Profil((int) document.get("experience"),
                            (int) document.get("niveau"),
                            (int) document.get("vie"),
                            (int) document.get("force"),
                            (int) document.get("defense"),
                            (int) document.get("etage"),
                            (Date) document.get("date"));
                    profil = recup;
                }

            }

            if(params[0] == "getMonstre"){
                Document document = monstres.find(and(eq("idPhone", id),eq("idPerso", idPerso))).first();

                if(document != null) {
                    Monster recup = new Monster((String) document.get("nom"),
                            (int) document.get("experience"),
                            (int) document.get("niveau"),
                            (int) document.get("vie"),
                            (int) document.get("force"),
                            (int) document.get("defense"),
                            (String) document.get("image"),
                            (Date) document.get("date"));
                    monstre = recup;
                }
            }

            return "some message";
        }

        @Override
        protected void onPostExecute(String message) {

        }
    }
}


