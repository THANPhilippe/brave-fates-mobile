package com.example.phili.jeux.modele;

import com.example.phili.jeux.controleur.Controle;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by phili on 24/12/2016.
 */

public class Monster {
    public String getNom() {
        return nom;
    }

    int vie;
    int force;
    int defense;
    int level;
    String nom;
    Date dateCreation;

    public int getExp() {
        return exp;
    }

    int exp;

    public String getImage() {
        return image;
    }

    String image;

    public int getForce() {
        return force;
    }

    public void setVie(int vie) {
        this.vie = vie;
    }

    public int getDefense() {
        return defense;
    }

    public int getVie() {
        return vie;
    }

    public int getLevel() {
        return level;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public Monster (String nom, int exp, int level, int vie, int force, int defense, String image, Date dateCreation){
        this.level = level;
        this.vie = vie;
        this.force = force;
        this.defense = defense;
        this.image = image;
        this.exp = exp;
        this.nom = nom;
        this.dateCreation = dateCreation;
    }
}
