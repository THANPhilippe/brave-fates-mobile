package com.example.phili.jeux.modele;

import java.util.Date;

/**
 * Created by phili on 24/12/2016.
 */

public class Profil {

    public int getForce() {
        return force;
    }

    public int getVie() {
        return vie;
    }

    public int getDefense() {
        return defense;
    }

    private int vie;
    private int force;
    private int defense;
    private int level;

    public Date getDateCreation() {
        return dateCreation;
    }

    private Date dateCreation;


    public void setVie(int vie) {
        this.vie = vie;
    }

    public void setForce(int force) {
        this.force = force;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public int getFloor() {
        return floor;
    }

    private int floor;

    public int getExperience() {
        return experience;
    }

    private int experience;

    public int getLevel() {
        return level;
    }

    public Profil(int experience, int level, int vie, int force, int defense, int floor, Date dateCreation){
        this.experience = experience;
        this.level= level;
        this.vie = vie;
        this.force = force;
        this.defense = defense;
        this.floor = floor;
        this.dateCreation = dateCreation;
    }


}
