package com.example.phili.jeux.vue;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.example.phili.jeux.R;

public class MainActivity extends AppCompatActivity {

    private void creerMenu(){

        ecouteMenu((ImageButton) findViewById(R.id.imageButton3), HeroActivity.class);

        SharedPreferences.Editor editor = getSharedPreferences("MES_PREFS",MODE_PRIVATE).edit();
        editor.putInt("Personnage", 1);
        editor.apply();

    }

    private void ecouteMenu(ImageButton btn, final Class classe){
        (btn).setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, classe);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        creerMenu();
    }


}
